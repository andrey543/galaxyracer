package com.admiral.galaxyracer.desktop;

import com.admiral.galaxyracer.Debugger;
import com.admiral.galaxyracer.Interfaces.Score;

public class ScoreListener implements Score {

    @Override
    public void writeRecord(String  score) {
        Debugger.log("WRITE RECORD = " + score);
    }

    @Override
    public String readRecord() {
        Debugger.log("READ RECORD");
        return "0,0";
    }
}
