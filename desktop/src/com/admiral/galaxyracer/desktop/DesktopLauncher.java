package com.admiral.galaxyracer.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.admiral.galaxyracer.MyGame;
import com.admiral.galaxyracer.Settings;


public class DesktopLauncher {
	private static final boolean CREATE_ATLAS = false;
	private static final boolean OS_LINUX = false;

	private static final String INPUT_LINUX_PATH = "/home/andrey/packages/spacedriver/desktop/src/assets";
	private static final String OUTPUT_LINUX_PATH = "/home/andrey/packages/spacedriver/android/assets/";
	private static final String INPUT_WINDOWS_PATH = "C:\\Users\\dmitr\\packages\\spacedriver\\desktop\\src\\assets";
	private static final String OUTPUT_WINDOWS_PATH = "C:\\Users\\dmitr\\packages\\spacedriver\\android\\assets";

	public static void main (String[] arg) {
		if(CREATE_ATLAS)
		{
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.edgePadding = true;
			settings.paddingX = 2;
			settings.paddingY = 2;

			String input;
			String output;
			if(OS_LINUX) {
				input  = INPUT_LINUX_PATH;
				output = OUTPUT_LINUX_PATH;
			} else {
				input  = INPUT_WINDOWS_PATH;
				output = OUTPUT_WINDOWS_PATH;
			}
			TexturePacker.process(settings, input, output, "atlas");

		} else {
			System.setProperty("user.name", "\\xD0\\x92\\xD0\\xBE\\xD0\\xB6\\xD0\\xB4\\xD1\\x8C");
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			Settings.android = false;

			config.title = "Space Invasion";
			config.width = Settings.screen_width;
			config.height = Settings.screen_height;

			MyGame game = new MyGame();
			game.addScoreListener(new ScoreListener());
			new LwjglApplication(game, config);
		}
	}


}
