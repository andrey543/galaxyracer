package com.admiral.galaxyracer;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication implements com.admiral.galaxyracer.Interfaces.Score {

	private MyGame game;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useCompass = false;
		Settings.android = true;

		this.game = new MyGame();
		this.game.addScoreListener(this);

		initialize(game, config);
	}

	@Override
	public void onBackPressed()
	{
		if(game.canExit()) {
			super.onBackPressed();
			game.dispose();
			//super.exit();
		}
		else
			game.pause();
	}

	@Override
	public void writeRecord(String score) {
	    // Получаем доступ к данным приложения( MODE_PRIVATE - доступ к данным только этого приложения)
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        // Получаем редактора данных
        Editor editor = sharedPreferences.edit();
        // Записываем рекорд в переменную RECORD
        editor.putString("RECORD", score);
        // Фиксируем запись
        editor.commit();

	}

	@Override
	public String readRecord() {
		SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
	    return sharedPreferences.getString("RECORD","-1");
	}
}
