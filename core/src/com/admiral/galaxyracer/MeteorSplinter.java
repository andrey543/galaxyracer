package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Pool;

import java.util.ArrayList;
import java.util.Random;

public class MeteorSplinter extends com.admiral.galaxyracer.MySprite implements com.admiral.galaxyracer.Interfaces.Splinter, Pool.Poolable {

    public static final Pool<MeteorSplinter> meteorSplinterPool = new Pool<MeteorSplinter>() {
        @Override
        protected MeteorSplinter newObject() {
            return new MeteorSplinter();
        }
    };
    private static final ArrayList<GraphicsContainer> graphics = new ArrayList<GraphicsContainer>();
    private static final float fireTimeAlive = 115;
    private float frequencyFire = 50;

    static 
    {
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",1),"MeteorBurnMedium"));
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",2),"MeteorBurnMedium"));
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",3),"MeteorBurnMedium"));
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",4),"MeteorBurnSmall"));
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",5),"MeteorBurnSmall"));
        graphics.add(new GraphicsContainer(com.admiral.galaxyracer.Controller.atlas.findRegion("splinters/meteor_splinter1",6),"MeteorBurnSmall"));
    }

    public Polygon polygon;
    public Intersector.MinimumTranslationVector intersectPoint = new Intersector.MinimumTranslationVector();
    public float power;

    private SpriteBatch screen = com.admiral.galaxyracer.Meteor.screen;
    private Random random = new Random();
    private GraphicsContainer container;
    private float fireTime;
    private boolean fireAlive;
    private float speedX, speedY;
    private boolean isPush;
    private float rotation;
    private float stepRotate;

    public MeteorSplinter()
    {
        this.container = graphics.get(random.nextInt(graphics.size()));
        this.polygon = new Polygon(new float[]{0, 0,
                this.container.img.getRegionWidth(),0, this.container.img.getRegionWidth(),
                this.container.img.getRegionHeight(), 0, this.container.img.getRegionHeight()});

    }

    @Override
    public void update()
    {
        if(this.polygon.getY() + this.container.img.getRegionHeight() <= 0
                || this.polygon.getY() > com.admiral.galaxyracer.Settings.screen_height
                || this.polygon.getX() > com.admiral.galaxyracer.Settings.screen_width
                || this.polygon.getX() + this.container.img.getRegionWidth() <= 0)
        this.alive = false;

        float newX = this.polygon.getX() + speedX * Gdx.graphics.getDeltaTime();
        float newY = this.polygon.getY() + speedY * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(newX,newY);
        screen.draw(container.img,this.polygon.getX(),this.polygon.getY(),
                container.img.getRegionWidth()/2,container.img.getRegionHeight()/2,
                container.img.getRegionWidth(), container.img.getRegionHeight(),
                1,1,rotation);
        this.rotation += stepRotate;
        if(fireAlive) fire();


    }

    private void fire()
    {
        this.fireTime += Gdx.graphics.getDeltaTime() * 1000;
        if(this.fireTime >= frequencyFire)
        {
            if(frequencyFire > fireTimeAlive)
                this.fireAlive = false;
            // TODO Поменять анимацию огня
            com.admiral.galaxyracer.Controller.animations.createAnimation(container.animation, polygon.getX(),polygon.getY());
            this.frequencyFire += 5;
            this.fireTime = 0;
        }
    }
    public void push()
    {
        //TODO Релизовать толчоек осколков от щита
        if(!isPush) {
            this.isPush = true;
            speedX += speedX * intersectPoint.normal.x;
            speedY *= -1;
        }

    }
    public void explosion()
    {
        // TODO Поменять анимацию столкновения
        com.admiral.galaxyracer.Controller.animations.createMeteorBurn(container.animation, 70, polygon.getX(), polygon.getY());
        this.alive = false;
    }

    @Override
    public void init(float x, float y, float speedX, float speedY) {

        this.alive = true;
        this.fireAlive = true;
        this.isPush = false;
        this.stepRotate = (random.nextInt(9) + 3) * random.nextFloat() * (random.nextInt(2) == 0 ? -1 : 1);
        this.frequencyFire += random.nextInt(35);
        this.polygon.setPosition(x,y);
        this.power = 0.3f + random.nextFloat();

        this.speedX = speedX;
        this.speedY = speedY;
    }

    @Override
    public void reset()
    {
        polygon.setPosition(0,0);

        this.power = 0;
        this.frequencyFire = 50;
        this.speedX = 0;
        this.speedY = 0;
    }

    private static class GraphicsContainer
    {
        TextureRegion img;
        String animation;

        GraphicsContainer(TextureRegion img, String animationName)
        {
            this.img = img;
            this.animation = animationName;
        }
    }

}
