package com.admiral.galaxyracer;

/**
 * Created by Вождь on 09.08.2017.
 */

public class Settings {
    /*
    screen_width и screen_height:
    - для ПК - 800 на 480,
    - для андройд Gdx.graphics.getWidth | getHeight
     */
    public static boolean android = true;
    public final static int screen_width = 1920; //Gdx.graphics.getWidth();
    public final static int screen_height = 1080; //Gdx.graphics.getHeight();

    public final static double stepTime = 0.05;

    public final static int shipStartX = screen_width/2;
    public final static int shipStartY = 29;
    public final static int shipSpeed = 450;
    public final static int shipShieldAliveTime = 5;
    public final static float shipHealth = 5;
    
    public final static int meteorSpeed = 550;            // 550
    public final static int meteorSpeedStep = 5;
    public final static int meteorAllowed = 10;          // 100
    public final static int meteorFrequency = 10;

    public final static float meteorLargeHealth = 7;
    public final static float meteorMediumHealth = 3;
    public final static float meteorSmallHealth = 2;
    public final static int countMeteorTimers = 5;
    public final static int stepMeteorTimers = 70;
    public final static int meteorAllowedIncInterval = 7;  // через сколько сек. будет увеличиватся макс.допустимое кол-во метеоров

    public final static float bonusPower = 0.17f;
    public final static int bonusIntervalGeneration = 7;

    public final static float frame_time = 45;

    // Вершины для полигонов(возможно нужно будет упаковать как то по другому)
    public final static float[] shipShieldVertices = new float[]{0,59,1,48,6,34,28,12,53,3,75,1,101,3,127,15,142,31,148,44,148,60};
    public final static float[] shipVertices = new float[]{33,10,27,4,21,4,9,16,21,16,0,30,0,43,21,63,24,82,33,62,41,82,45,63,65,43,65,30,45,16,57,16,44,4,38,27};
    public final static float[] largeMeteorVertices1 = new float[]{93,140,55,136,28,127,0,91,0,73,3,66,3,58,20,42,28,28,42,22,49,13,78,1,100,0,107,5,121,5,137,22,147,31,146,53,144,63,139,73
    ,140,85,129,98,125,116,118,126};
    public final static float[] largeMeteorVertices2 = new float[]{77,159,44,148,23,120,12,114,0,82,4,67,17,56,37,18,60,3,72,2,98,12,124,29,146,83,143,105,109,147,94,152};
    public final static float[] largeMeteorVertices3 = new float[]{70,150,48,146,31,138,23,130,22,120,10,106,9,97,1,80,4,59,19,26,39,8,87,1,98,6,108,10,121,23,129,36,131,43,137,75,132,103,112,130,78,148};
    public final static float[][] arrayLargeMeteorVerts = new float[][]{largeMeteorVertices1,largeMeteorVertices2,largeMeteorVertices3};


}
