package com.admiral.galaxyracer;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Вождь on 25.08.2017.
 */

public class Joystick {

    private SpriteBatch screen;
    //координаты центра джостика
    private float pointX;
    private float pointY;
    //перевернутые Y координаты джостика( т.к. Y координаты начинаются с верхну )
    private float pointYReverse;
    // скоростные коэффициенты
    private float speedKX;
    private float speedKY;
    // процентное соотношение(ширина текстуры джостика/коор.нажатия) для вычисления скор.коэфф.
    private double per;
    private Ship ship;
    private Settings settings;
    public Texture texture;

    public Joystick(SpriteBatch batch, Ship ship, Settings settings)
    {
        texture = new Texture("joistick.jpg");
        this.screen = batch;
        this.ship = ship;
        this.settings = settings;
        pointX = 0;
        pointY = 0;
        pointYReverse = settings.screen_height - pointY;
        per = texture.getWidth() * 0.01;
    }

    public void draw()
    {
        screen.draw(texture, pointX - texture.getWidth()/2 ,pointY - texture.getHeight()/2);
    }
    public void touchPosition(float x, float y)
    {

        if(x > pointX) {
            // обработка работы дожстика с вычислением скоротного коэффициента
            if(x < pointX + texture.getWidth()) {
                speedKX = x - pointX;
                speedKX /= per * 100;
                //System.out.println("SpeedK right= "+ speedKX);
                ship.moveJoyRight(speedKX);
            }
            else
                ship.moveRight();
        }
        else if(x < pointX) {
            if(x > pointX - texture.getWidth()) {
                speedKX = pointX - x;
                speedKX /= per * 100;
                //System.out.println("SpeedK left= "+ speedKX);
                ship.moveJoyLeft(speedKX);
            }
            else
                ship.moveLeft();
        }
        if(y < pointYReverse) {
            // обработка работы дожстика с вычислением скоротного коэффициента
            if(y > pointYReverse - texture.getHeight()) {
                speedKY = pointYReverse - y;
                speedKY /= per * 100;
                //System.out.println("SpeedK up= "+ speedKY);
                ship.moveJoyDown(speedKY);
            }
            else
                ship.moveDown();
        }
        else if(y > pointYReverse) {
            if(y < pointYReverse + texture.getWidth()) {
                speedKY = y - pointYReverse;
                speedKY /= per * 100;
                //System.out.println("SpeedK down= "+ speedKY);
                ship.moveJoyUp(speedKY);
            }
            else
                ship.moveUp();
        }
    }
    public boolean joyZone(float x, float y)
    {
        if(x < settings.screen_width/2)
            return true;
        else
            return false;
    }
}
