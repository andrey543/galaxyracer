package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Вождь on 26.05.2018.
 */

/** *
 * Класс в основном для отрисовки полигонов, и прямоугольников
 * ВНИМАНИЕ! Некоторые методы имеют утечки памяти
 * Уже не имеют
 */

public class Debugger
{

    private final static BitmapFont font;
    private final static DecimalFormat decimalFormat = new DecimalFormat("##0.00000");
    private final static ShapeRenderer shapeRenderer = new ShapeRenderer();
    private final static GLProfiler glProfiler = new GLProfiler(Gdx.graphics);

    static
    {
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/digital.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 40;

        font = generator.generateFont(parameter);
        font.setColor(0.93f,0.95f,0.2f,1f);

        generator.dispose();

    }
    public static BitmapFont createFont(float r, float g, float b, float a, int size)
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/digital.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;

        BitmapFont font = generator.generateFont(parameter);
        font.setColor(r,g,b,a);

        generator.dispose();

        return font;
    }
    public static BitmapFont createFont()
    {
        return createFont(0.93f,0.95f,0.2f,1f, 58);
    }
    public static BitmapFont createFont(int size)
    {
        return createFont(0.93f,0.95f,0.2f,1f, size);
    }
    public static void profile(SpriteBatch screen, float x, float y)
    {
        if(glProfiler.isEnabled()) {
            drawLog("[FPS]: ", Gdx.graphics.getFramesPerSecond(), screen, x, y);
            drawLog("[TEXTURE BINDINGS]: ", glProfiler.getTextureBindings(), screen, x, y - 50);
            drawLog("[CALLS]: ", glProfiler.getCalls(), screen, x, y - 100);
            drawLog("[SHADER SWITCHES]: ", glProfiler.getShaderSwitches(), screen, x, y - 150);
            //drawLog("[JAVA HEAP]: ", Gdx.app.getJavaHeap(), screen, x, y - 150);
            //drawLog("[NATIVE HEAP]: ", Gdx.app.getNativeHeap(), screen, x, y - 200);
            glProfiler.reset();
        }
        else
            glProfiler.enable();

    }

    public static void log(Object log)
    {
        System.out.println(log.toString());
    }
    public static void drawLog(String text,int value, SpriteBatch screen, float x, float y)
    {
        font.draw(screen, text + value, x, y);
    }
    public static void drawLog(String text,float value, SpriteBatch screen, float x, float y)
    {
        font.draw(screen, text + value, x, y);
    }
    public static void drawLog(String text,double value, SpriteBatch screen, float x, float y)
    {
        font.draw(screen, text + value, x, y);
    }
    public static void drawLog(String text,boolean value, SpriteBatch screen, float x, float y)
    {
        font.draw(screen, text + value, x, y);
    }
    public static void drawLog(String text, String value, SpriteBatch screen, float x, float y)
    {
        font.draw(screen, text + value, x, y);
    }

    /* Ручная отрисовка полигона */
    public static void drawPolygon(Polygon polygon, TextureRegion img, SpriteBatch screen) {
        Pixmap pixmap= new Pixmap(img.getRegionWidth(), img.getRegionHeight(), Pixmap.Format.RGBA8888);
        Pixmap pixmap1 = new Pixmap(img.getRegionWidth(), img.getRegionHeight(), Pixmap.Format.RGBA8888);
        drawingPolygons(false,pixmap,pixmap1,polygon,screen);
    }
    public static void drawPolygon(Polygon polygon, Texture img, SpriteBatch screen) {
        Pixmap pixmap = new Pixmap(img.getWidth(), img.getHeight(), Pixmap.Format.RGBA8888);
        Pixmap pixmap1 = new Pixmap(img.getWidth(), img.getHeight(), Pixmap.Format.RGBA8888);
        drawingPolygons(false,pixmap,pixmap1,polygon,screen);
    }
    public static void drawPolygonWithBackground(Polygon polygon, TextureRegion img, SpriteBatch screen) {
        Pixmap pixmap= new Pixmap(img.getRegionWidth(), img.getRegionHeight(), Pixmap.Format.RGBA8888);
        Pixmap pixmap1 = new Pixmap(img.getRegionWidth(), img.getRegionHeight(), Pixmap.Format.RGBA8888);
        drawingPolygons(true,pixmap,pixmap1,polygon,screen);
    }
    public static void drawPolygonWithBackground(Polygon polygon, Texture img, SpriteBatch screen) {
        Pixmap pixmap = new Pixmap(img.getWidth(), img.getHeight(), Pixmap.Format.RGBA8888);
        Pixmap pixmap1 = new Pixmap(img.getWidth(), img.getHeight(), Pixmap.Format.RGBA8888);
        drawingPolygons(true,pixmap,pixmap1,polygon,screen);
    }
    public static String formatDouble(double value)
    {
        return decimalFormat.format(value);
    }

    private static void drawingPolygons(boolean withBg, Pixmap pixmap, Pixmap pixmap1, Polygon polygon, SpriteBatch screen)
    {
        pixmap.setColor(0, 1, 0, 0.75f);
        pixmap1.setColor(1,1,1,0.2f);
        pixmap1.fill();

        float[] points = polygon.getVertices();
        ArrayList<Float> listPoints = new ArrayList<Float>();
        for(float x : points)
            listPoints.add(x);

        Iterator<Float> iterator = listPoints.iterator();
        float x1 = iterator.next();
        float y1 = iterator.next();
        while(iterator.hasNext())
        {
            float x2 = iterator.next();
            float y2 = iterator.next();

            pixmap.drawLine((int)x1,(int)(pixmap.getHeight() - y1),(int)x2,(int)(pixmap.getHeight() - y2));

            x1 = x2;
            y1 = y2;

        }
        Texture polygonImg = new Texture(pixmap);
        Texture mapPolygon = new Texture(pixmap1);
        if(withBg)
            screen.draw(mapPolygon,polygon.getX(),polygon.getY());
        screen.draw(polygonImg,polygon.getX(), polygon.getY());
    }
    /* Автоматическая отрисовка полигона(закрашивает экран и вообще работает странно О_о) */
    public static void drawDebug(Polygon polygon) {
        shapeRenderer.setProjectionMatrix(com.admiral.galaxyracer.MyGame.camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.polygon(polygon.getTransformedVertices());
        shapeRenderer.end();
    }
    /* Отрисовка прямоугольника */
    public static void drawRect(Rectangle rect, SpriteBatch screen)
    {
        Pixmap pixmap = new Pixmap((int)rect.getWidth(),(int)rect.getHeight(),Pixmap.Format.RGBA8888);
        pixmap.setColor(0, 1, 0, 0.75f);
        pixmap.fillRectangle(0,0, (int)rect.width, (int)rect.height);
        Texture rectImg = new Texture(pixmap);

        screen.draw(rectImg, rect.x,rect.y);
    }
    public static float[] reverseVerticesY(float[] vertices, float height)
    {
        for(int i = 0; i < vertices.length; i++)
            if(i % 2 == 1)
                vertices[i] = Math.abs(vertices[i] - height);

        return vertices;
    }
    //TODO Следует сделать у отладчика возможность менять грани полигона в риал тайм
    //private static int point = 1;
//    public void setPointPolygon(int p)
//    {
//        if(p>= 0 && p <= 6)
//            point = p;
//    }
//    public void setSize(boolean up)
//    {
//        float[] vertices = polygon.getVertices();
//        vertices[point] += (up) ? 1 : -1;
//        polygon.setVertices(vertices);
//    }
}
