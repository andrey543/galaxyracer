package com.admiral.galaxyracer;



public abstract class StateMachine {
    enum States
    {
        Stoped,Running,Paused
    }

    States activeState;

    void setActiveState(States state)
    {
        this.activeState = state;
    }

    public abstract void stop();
    public abstract void start();
    public abstract void pause();
    public abstract void update();
}
