package com.admiral.galaxyracer;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Timer;

/**
 * Класс силового поля корабля
*/

public class ShipShield {

    private static Texture img = new Texture("shield.png");

    private boolean alive;
    private Polygon polygon = new Polygon(Debugger.reverseVerticesY(com.admiral.galaxyracer.Settings.shipShieldVertices,img.getHeight()));
    private SpriteBatch screen;
    private com.admiral.galaxyracer.Ship ship;
    private Timer aliveTimer = new Timer();
    private int aliveTime;

    public ShipShield(SpriteBatch screen, com.admiral.galaxyracer.Ship ship)
    {
        this.screen = screen;
        this.ship = ship;

        Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                aliveTime++;
            }
        };
        aliveTimer.scheduleTask(task,1,1);
        aliveTimer.stop();
    }

    public Polygon getPolygon() {
        return polygon;
    }
    public float getTextureWidth()
    {
        return img.getWidth();
    }
    public float getTextureHeight()
    {
        return img.getHeight();
    }

    public boolean isAlive()
    {
        return this.alive;
    }
    public void activate()
    {
        this.alive = true;
        this.aliveTimer.start();
    }
    public void pause()
    {
        if(alive)
            this.aliveTimer.stop();
    }
    public void goOn()
    {
        if(alive)
            this.aliveTimer.start();
    }
    public void deactivate()
    {
        this.alive = false;
        this.aliveTimer.stop();
        this.aliveTime = 0;
    }
    public void update()
    {
        if(this.aliveTime > com.admiral.galaxyracer.Settings.shipShieldAliveTime)
            deactivate();

        if(!this.alive)
            return;

        this.polygon.setPosition(ship.polygon.getX() - (img.getWidth() - this.ship.getTextureWidth())/2,
                                 ship.polygon.getY() + (ship.getTextureHeight() + img.getHeight()/3));
        //Debugger.drawDebug(polygon);
        screen.draw(img, this.polygon.getX(), this.polygon.getY());
    }


}
