package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Sky {
    private static Sky instance;
    private static float moveStep = 3;

    private SpriteBatch screen;
    private Background bg1 = new Background("5.jpg",0,0);
    private Background bg2 = new Background("5.jpg", 0, bg1.y + bg1.img.getHeight());

    private Sky(SpriteBatch screen)
    {
        this.screen = screen;
    }
    public static Sky getInstance(SpriteBatch screen)
    {
        if(instance == null)
            instance = new Sky(screen);
        return instance;
    }

    private void moveBackground()
    {
        if(bg1.y <= -bg1.img.getHeight()) bg1.y = bg2.y + bg2.img.getHeight();
        if(bg2.y <= -bg2.img.getHeight()) bg2.y = bg1.y + bg1.img.getHeight();

        bg1.y -= com.admiral.galaxyracer.Controller.speed * Gdx.graphics.getDeltaTime() * 0.3;
        bg2.y -= com.admiral.galaxyracer.Controller.speed * Gdx.graphics.getDeltaTime() * 0.3;
    }

    public void update()
    {
        moveBackground();
        screen.draw(bg1.img, bg1.x, bg1.y);
        screen.draw(bg2.img, bg2.x, bg2.y);
    }
    public void pause()
    {
        screen.draw(bg1.img, bg1.x, bg1.y);
        screen.draw(bg2.img, bg2.x, bg2.y);
    }

    private class Background
    {
        private float x,y;
        private Texture img;

        Background(String path, float x, float y)
        {
            this.img = new Texture(path);
            this.x = x;
            this.y = y;
        }
    }


}
