package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Pool;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Вождь on 29.09.2017.
 */


public class Meteor extends com.admiral.galaxyracer.MySprite implements Pool.Poolable{

    enum TYPES
    {
        SMALL, MEDIUM, LARGE
    }

    public static  SpriteBatch screen;

    private static ArrayList<Sound> largeMeteorSounds = new ArrayList<Sound>();
    private static ArrayList<Sound> mediumMeteorSounds = new ArrayList<Sound>();
    private static ArrayList<Sound> smallMeteorSounds = new ArrayList<Sound>();
    private static ArrayList<TextureRegion> imgsMeteorLarge;
    private static ArrayList<TextureRegion> imgsMeteorMedium;
    private static ArrayList<TextureRegion> imgsMeteorSmall;

    static
    {
        imgsMeteorLarge  = new ArrayList<TextureRegion>();
        imgsMeteorMedium = new ArrayList<TextureRegion>();
        imgsMeteorSmall  = new ArrayList<TextureRegion>();

        largeMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/large_meteor_burn.ogg")));
        largeMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/large1.mp3")));
        largeMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/large2.mp3")));
        largeMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/large3.mp3")));

        mediumMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/medium1.mp3")));

        smallMeteorSounds.add(Gdx.audio.newSound(Gdx.files.internal("sounds/small1.mp3")));

        imgsMeteorLarge.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor1", 1));
        imgsMeteorLarge.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor1", 2));
        imgsMeteorLarge.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor1", 3));

        imgsMeteorMedium.add(com.admiral.galaxyracer.Controller.atlas.findRegion("meteor2", 1));
        imgsMeteorMedium.add(com.admiral.galaxyracer.Controller.atlas.findRegion("meteor2", 2));
        imgsMeteorMedium.add(com.admiral.galaxyracer.Controller.atlas.findRegion("meteor2", 3));

        imgsMeteorSmall.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor3", 1));
        imgsMeteorSmall.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor3", 2));
        imgsMeteorSmall.add (com.admiral.galaxyracer.Controller.atlas.findRegion("meteor3", 3));

    }

    public Polygon polygon;
    public static Pool<Meteor> meteorPool = new Pool<Meteor>() {
        @Override
        protected Meteor newObject() {
            return new Meteor();
        }
    };

    private Random random = new Random();
    private TYPES type;
    private String typeExplosion;
    private TextureRegion img;
    private Sound soundDistruction;
    private float rotation;
    private float stepRotate;
    private float health;
    private boolean isPushBack;
    private float pushX;
    private float valueReverseSpeed = 1f;
    private float volume = 0.5f;

    public Meteor()
    {
        this.type = TYPES.values()[random.nextInt(TYPES.values().length)];

        if(this.type == TYPES.LARGE)
        {
            int number = random.nextInt(imgsMeteorLarge.size());
            this.typeExplosion    = "MeteorBurnLarge" + (number + 1);
            this.img = imgsMeteorLarge.get(number);
            this.polygon = new Polygon(com.admiral.galaxyracer.Debugger.reverseVerticesY(com.admiral.galaxyracer.Settings.arrayLargeMeteorVerts[number].clone(), img.getRegionHeight()));
            this.volume = 0.8f;

        }
        else if(this.type == TYPES.MEDIUM)
        {
            int number = random.nextInt(imgsMeteorMedium.size());
            this.typeExplosion = "MeteorBurnMedium";
            this.img = imgsMeteorMedium.get(number);
            this.polygon = new Polygon(new float[]{0, 0, img.getRegionWidth(), 0, img.getRegionWidth(), img.getRegionHeight(), 0, img.getRegionHeight()});
            this.volume = 0.7f;
        }
        else
        {
            int number = random.nextInt(imgsMeteorSmall.size());
            this.typeExplosion = "MeteorBurnSmall";
            this.img = imgsMeteorSmall.get(number);
            this.polygon = new Polygon(new float[]{0, 0, img.getRegionWidth(), 0, img.getRegionWidth(), img.getRegionHeight(), 0, img.getRegionHeight()});
            this.volume = 0.15f;
        }
        this.polygon.setOrigin(this.img.getRegionWidth()/2, this.img.getRegionHeight()/2);
    }

    public float getTextureWidth()
    {
        return this.img.getRegionWidth();
    }
    public float getTextureHeight()
    {
        return this.img.getRegionHeight();
    }

    public String getTypeExplosion() {
        return typeExplosion;
    }

    public boolean isPushBack() {
        return isPushBack;
    }

    public TYPES getType()
    {
        return this.type;
    }
    public float getHealth()
    {
        return this.health;
    }

    public void setPushX(float pushX) {
        this.pushX = pushX;
    }

    public void setValueReverseSpeed(float valueReverseSpeed) {
        this.valueReverseSpeed = valueReverseSpeed;
    }

    public void decreaseHealth(float power )
    {
        this.health -= power ;
    }

    private void explosion()
    {
        this.alive = false;

        if(this.type == TYPES.LARGE) {
            com.admiral.galaxyracer.Controller.animations.createMeteorBurn(this.typeExplosion, 65, this.polygon.getX() - 80, this.polygon.getY() - 60);
            if(random.nextInt(3) == 2) // 33% шанс выпадения
                com.admiral.galaxyracer.Controller.createBonus(this.polygon.getX() + this.getWidth()/2, this.polygon.getY() - this.getHeight()/2);
            com.admiral.galaxyracer.Controller.splinterFabric.createSplinters(
                    "MeteorSplinter",
                    this.polygon.getX() + this.getWidth()/2,
                    this.polygon.getY() - this.getHeight()/2,
                    5 + random.nextInt(10));
        }
        else if(this.type == TYPES.MEDIUM) {
            com.admiral.galaxyracer.Controller.animations.createMeteorBurn(this.typeExplosion, 70, this.polygon.getX() - 10, this.polygon.getY() - 10);
            if(random.nextInt(10) == 9) // 10% шанс выпадения
                com.admiral.galaxyracer.Controller.createBonus(this.polygon.getX() + this.getWidth()/2, this.polygon.getY() - this.getHeight()/2);
        }
        else
            com.admiral.galaxyracer.Controller.animations.createMeteorBurn(this.typeExplosion,70,this.polygon.getX() - 13, this.polygon.getY() - 13);

        if(this.soundDistruction != null)
        {
            this.soundDistruction.play(volume);
        }
    }
    public void pushBack(float pointX)
    {
        this.setValueReverseSpeed(-1);
        float meteorPointX = this.polygon.getX() + this.getTextureWidth()/2;
        float value = pointX - meteorPointX;
        float minus = (value < 0) ? -1 : 1;
        value = (float) (Math.pow(value,2)/13) * minus;
        this.setPushX(-value);

        this.isPushBack = true;
    }


    public void init(float x, float y)
    {
        this.alive = true;
        this.polygon.setPosition(x,y);
        this.stepRotate = random.nextFloat() * (random.nextInt(3) - 1);

        if(this.type == TYPES.LARGE) {
            this.health = com.admiral.galaxyracer.Settings.meteorLargeHealth + random.nextFloat() * 5;
            this.soundDistruction = largeMeteorSounds.get(random.nextInt(largeMeteorSounds.size()));
        }
        else if(this.type == TYPES.MEDIUM) {
            this.health = com.admiral.galaxyracer.Settings.meteorMediumHealth + random.nextFloat() * 3;
            this.soundDistruction = mediumMeteorSounds.get(random.nextInt(mediumMeteorSounds.size()));
        }
        else {
            this.health = random.nextFloat() * 2 + 1.5f;
            this.soundDistruction = smallMeteorSounds.get(random.nextInt(smallMeteorSounds.size()));
        }

        //Debugger.log(this.health);
    }
    @Override
    public void reset()
    {
        this.polygon.setPosition(0,0);
        this.isPushBack = false;
        this.pushX = 0;
        this.valueReverseSpeed = 1;
        this.alive = false;
    }
    @Override
    public void update()
    {
        if(health <= 0 && alive)
            explosion();

        if(this.polygon.getY() + this.img.getRegionHeight() <= 0)
            this.alive = false;
        if(isPushBack && (this.polygon.getX() + getTextureWidth() <= 0 || this.polygon.getX() >= com.admiral.galaxyracer.Settings.screen_width
                || this.polygon.getY() >= com.admiral.galaxyracer.Settings.screen_height)) {
            this.alive = false;
        }

        float newY = this.polygon.getY() - (com.admiral.galaxyracer.Controller.speed * valueReverseSpeed) * Gdx.graphics.getDeltaTime();
        float newX = this.polygon.getX() + this.pushX * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(newX, newY);
        this.polygon.setRotation(rotation);

        screen.draw(this.img,this.polygon.getX(),this.polygon.getY(),
                this.img.getRegionWidth()/2,this.img.getRegionHeight()/2,
                this.img.getRegionWidth(), this.img.getRegionHeight(),
                1,1,rotation);
        this.rotation += this.stepRotate;
        //Debugger.drawPolygonWithBackground(this.polygon,this.img,screen);
        //Debugger.drawDebug(this.polygon);
    }
}
