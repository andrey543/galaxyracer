package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Pool;



/**
 * Created by Вождь on 09.09.2017.
 */

public class ShipBullet extends MySprite implements Pool.Poolable {


    public static SpriteBatch screen;

    // Новое решение модернизации(при активации бонуса)
    protected static TextureRegion imgCommon = Controller.atlas.findRegion("bullets/bullet1");

    public static int speedLvl = 1000;
    public static float powerLvl = 0.95f;
    public static float allowHeatLvl = 10;
    public static float coolingTimeLvl = 100f;
    public static float frequencyLvl = 110f;

    protected TextureRegion img;
    public Polygon polygon;
    public int speed;
    public float power;
    public float allowHeat;
    public float coolingTime;
    public float frequency;

    public ShipBullet()
    {
        this.polygon = new Polygon();
    }

    public void init(ShipGun gun)
    {
        this.alive = true;
        this.img = imgCommon;

        this.polygon.setVertices(new float[]{0, 0, this.img.getRegionWidth(),0, this.img.getRegionWidth(), this.img.getRegionHeight(), 0, this.img.getRegionHeight()});
        this.polygon.setPosition(gun.getX() - img.getRegionWidth()/2, gun.getY() + img.getRegionHeight());
    }
    public static void gameOver()
    {
        speedLvl = 1000;
        powerLvl = 0.95f;
        allowHeatLvl = 10;
        coolingTimeLvl = 100f;
        frequencyLvl = 110f;
    }

    @Override
    public void reset()
    {
        this.img = null;
        this.polygon.setPosition(0,0);
        this.alive = false;

        this.speed = 0;
        this.allowHeat = 0;
        this.coolingTime = 0;
        this.frequency = 0;
    }

    @Override
    public void update()
    {
        this.speed = speedLvl;
        this.power = powerLvl;
        this.allowHeat = allowHeatLvl;
        this.coolingTime = coolingTimeLvl;
        this.frequency = frequencyLvl;

        if (this.polygon.getY() - this.img.getRegionHeight() >= Settings.screen_height)
            alive = false;

        float newY = this.polygon.getY() + this.speed * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(this.polygon.getX(), newY);

        screen.draw(img, polygon.getX(), polygon.getY());
    }

}
