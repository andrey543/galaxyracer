package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;

/**
 * Created by Андрей on 01.09.2018.
 */

public class EffectFreeze {

    public boolean alive;

    private com.admiral.galaxyracer.Ship ship;
    private int time;
    private int effectTime;
    private int currentControllerSpeed;
    private int currentShipBulletSpeed;
    private float currentShipBulletFreq;
    private float currentShipBulletCooling;
    private int currentShipSpeed;
    private boolean reverse;

    private final int valueFreeze = 70; // %
    private final int workTime = 3;     // сек
    private final int dt = 8;          // мсек

    public EffectFreeze(com.admiral.galaxyracer.Ship ship)
    {
        this.ship = ship;
    }

    public void start()
    {
        this.currentControllerSpeed = com.admiral.galaxyracer.Controller.viewSpeed;
        this.alive = true;
        this.currentShipBulletSpeed = com.admiral.galaxyracer.ShipBullet.speedLvl;
        this.currentShipBulletFreq = com.admiral.galaxyracer.ShipBullet.frequencyLvl;
        this.currentShipBulletCooling = com.admiral.galaxyracer.ShipBullet.coolingTimeLvl;
        this.currentShipSpeed = com.admiral.galaxyracer.Ship.speed;

        com.admiral.galaxyracer.Controller.startTimer.stop();
        com.admiral.galaxyracer.Controller.currentMeteorTimer.stop();
        com.admiral.galaxyracer.Controller.speedTimer.stop();
        this.ship.shield.pause();


    }
    public void stop()
    {
        this.alive = false;
        time                     = 0;
        effectTime               = 0;
        currentControllerSpeed   = 0;
        currentShipBulletSpeed   = 0;
        currentShipBulletFreq    = 0;
        currentShipBulletCooling = 0;
        currentShipSpeed         = 0;
        reverse                  = false;
        com.admiral.galaxyracer.Animation.freeze         = 0;
    }
    public void prolong()
    {
        this.effectTime = 0;
    }

    public void update()
    {
        int i = (time/dt);

        if(i < valueFreeze && !reverse)
            time += Gdx.graphics.getDeltaTime() * 1000;
        else {
            effectTime += Gdx.graphics.getDeltaTime() * 1000;
            if(effectTime > workTime * 1000) {
                reverse = true;
                effectTime = 0;
            }
        }

        if(reverse) {
            if(i > 0)
                time -= Gdx.graphics.getDeltaTime() * 1000;
            else
            {
                this.time = 0;
                this.reverse = false;
                this.alive = false;
                currentControllerSpeed = com.admiral.galaxyracer.Controller.viewSpeed;

                com.admiral.galaxyracer.Controller.startTimer.start();
                com.admiral.galaxyracer.Controller.currentMeteorTimer.start();
                com.admiral.galaxyracer.Controller.speedTimer.start();
                this.ship.shield.goOn();

            }
        }

        com.admiral.galaxyracer.Controller.speed = (int)(this.currentControllerSpeed - (this.currentControllerSpeed * (0.01f * i)));
        com.admiral.galaxyracer.ShipBullet.speedLvl = (int)(this.currentShipBulletSpeed - (this.currentShipBulletSpeed * (0.01f * i)));
        com.admiral.galaxyracer.ShipBullet.frequencyLvl = (int)(this.currentShipBulletFreq + (this.currentShipBulletFreq * (0.01f * i)));
        com.admiral.galaxyracer.ShipBullet.coolingTimeLvl = (int)(this.currentShipBulletCooling + (this.currentShipBulletCooling * (0.01f * i)));
        com.admiral.galaxyracer.Ship.speed = (int)(this.currentShipSpeed - (this.currentShipSpeed * (0.01f * i)));
        com.admiral.galaxyracer.Animation.freeze = i;
        //Debugger.drawLog("Vasya: ", i, screen, 0, Settings.screen_height/2);

    }

}
