package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;
/**
            Класс анимации
    Необходим для создания анимационных сцен такие
    как:
    - взрывы
    - столкновения
    - выстрелы и т.д.

    Для создания новой анимации:
    1.унаследоватся от этого класса
    2.передать в super-конструктор координаты анимации, кол-во кадров

 */

/**
 * Created by Вождь on 11.11.2017.
 */

public class Animation implements Pool.Poolable
{

    public static com.admiral.galaxyracer.AnimCatalog catalog = new com.admiral.galaxyracer.AnimCatalog();
    public static SpriteBatch screen;
    public static int freeze;

    private TextureRegion[] animMap;
    private float x;
    private float y;
    private float offsetX;
    private float offsetY;
    private float work_time;
    private int frame;
    private float frame_time;
    private int count_frames;
    private int speedDrop;
    private boolean looping;
    public String name;
    public boolean alive;
    public com.admiral.galaxyracer.Ship ship;

    public Animation()
    {
    }

//    //public boolean isLooping()
//    {
//        return this.looping;
//    }
    public void setLooping(boolean looping)
    {
        this.looping = looping;
    }
    public void setFrameTime(float time)
    {
        this.frame_time = time;
    }
    public void setCountFrames(int count)
    {
        this.count_frames = count;
    }
    public void setSpeedDrop(int speed)
    {
        this.speedDrop = speed;
    }
    public void fixedTo(com.admiral.galaxyracer.Ship ship, float offsetX, float offsetY)
    {
        this.ship = ship;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    @Override
    public void reset() {
        this.name         = null;
        this.animMap      = null;
        this.ship         = null;
        this.alive        = false;
        this.looping      = false;
        this.work_time    = 0;
        this.frame        = 0;
        this.x            = 0;
        this.y            = 0;
        this.offsetX      = 0;
        this.offsetY      = 0;
    }

    public void init(String name, float x, float y)
    {
        this.name = name;

        this.frame_time = com.admiral.galaxyracer.Settings.frame_time;
        this.speedDrop = com.admiral.galaxyracer.Controller.speed + 100;

        this.animMap = catalog.getAnimMap(name);
        this.count_frames = this.animMap.length;
        this.alive = true;
        this.x = x;
        this.y = y;

    }

    /************ Отрисовка ************/
    public void update()
    {
        //TODO КОСТЫЛЬ
        if(this.ship != null && this.ship.alive)
        {
            this.x = this.ship.polygon.getX() + this.offsetX;
            this.y = this.ship.polygon.getY() + this.offsetY;

        }

        work_time += Gdx.graphics.getDeltaTime() * 1000;
        if(work_time < (frame_time + freeze)* count_frames)
        {
            this.frame =(int)(work_time / (frame_time + freeze));
            screen.draw(animMap[this.frame],this.x, this.y);
            this.y -= (this.speedDrop - freeze) * Gdx.graphics.getDeltaTime();
        }
        else
        {
            if(looping)
                work_time = 0;
            else
                this.alive = false;
        }
    }
}
