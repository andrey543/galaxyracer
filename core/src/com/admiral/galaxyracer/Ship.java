package com.admiral.galaxyracer;

/**
 * Created by Вождь on 09.08.2017.
 */
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;

import java.util.ArrayList;
import java.util.Random;

public class Ship extends Sprite {

    // Общие
    public static int speed;
    private static Ship instance;
    private static Sound soundDistruction = Gdx.audio.newSound(Gdx.files.internal("sounds/ship_distruction.ogg"));

    // Свойства
    public Polygon polygon;
    public ShipGun gun1;
    public ShipGun gun2;
    public ShipShield shield;
    public boolean gameOver;
    public boolean alive;
    public Intersector.MinimumTranslationVector intersectPoint = new Intersector.MinimumTranslationVector();

    private SpriteBatch screen;
    private TextureRegion shipImg;
    private Random random = new Random();
    private ShipSplinters splinters = new ShipSplinters();
    private HealthIndicator healthIndicator = new HealthIndicator(new TextIndicator());
    private float health;

    private Ship(SpriteBatch screen)
    {
        this.screen = screen;
        this.shipImg = Controller.atlas.findRegion("ship5");
        this.polygon = new Polygon(Settings.shipVertices);
        this.gun1 = new ShipGun(this, this.shipImg.getRegionWidth()/5, 0);
        this.gun2 = new ShipGun(this, (this.shipImg.getRegionWidth()/5)*5, 0);
        this.shield = new ShipShield(this.screen,this);
        create();
    }
    public static Ship getInstance(SpriteBatch screen)
    {
        if(instance == null)
            instance = new Ship(screen);
        return instance;
    }

    public float getHealth()
    {
        return this.health;
    }
    public float getTextureWidth()
    {
        return this.shipImg.getRegionWidth();
    }
    public float getTextureHeight()
    {
        return this.shipImg.getRegionHeight();
    }

    public void update()
    {
        if(health <= 0 && alive)
            destruction();

        this.gun1.update();
        this.gun2.update();
        this.shield.update();
        this.healthIndicator.update();
        screen.draw(this.shipImg, this.polygon.getX(), this.polygon.getY());

        if(!this.alive)
            splinters.update();
    }
    public void create()
    {
        this.alive = true;
        this.health = Settings.shipHealth;
        speed = Settings.shipSpeed;
        ShipGun.bullets.clear();
        this.gun1.reset();
        this.gun2.reset();
        this.polygon.setPosition(Settings.shipStartX - shipImg.getRegionWidth()/2, Settings.shipStartY - shipImg.getRegionWidth());
        // TODO Придумать функцию преобразующую координаты смещения в зависимости от размера картинки анимации
        Controller.animations.createShipFire(this, this.shipImg.getRegionWidth() / 5 + 1, -29);
        Controller.animations.createShipFire(this, this.shipImg.getRegionWidth() / 2, -29);

    }
    public void decreaseHealth(float power)
    {
        this.health -= power;
        if(this.health < 0) this.health = 0;
    }
    public void destruction()
    {
        this.gameOver = true;
        float x = polygon.getX() - 10;
        float y = polygon.getY() - 34;
        Controller.animations.createShipBurn(x, y);
        Controller.speedTimer.stop();
        Controller.startTimer.stop();
        Controller.overTimer.start();
        soundDistruction.play();
        this.splinters.start(this.polygon.getX(), this.polygon.getY());
        this.alive = false;
        this.polygon.setPosition(0, -shipImg.getRegionHeight());
        this.shield.deactivate();
        Controller.animations.destructionAnimation("ShipFire");
    }
    public void moveRight()
    {
        if(this.polygon.getX() + this.shipImg.getRegionWidth() < Settings.screen_width) {
            float newX = this.polygon.getX() + speed * Gdx.graphics.getDeltaTime();
            this.polygon.setPosition(newX,this.polygon.getY());
        }
    }
    public void moveLeft()
    {
        if(this.polygon.getX() > 0) {
            float newX = this.polygon.getX() - speed * Gdx.graphics.getDeltaTime();
            this.polygon.setPosition(newX, this.polygon.getY());
        }
    }
    public void moveUp()
    {
        float newY = this.polygon.getY() + speed * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(this.polygon.getX(), newY);
    }
    public void moveDown()
    {
        float newY = this.polygon.getY() - speed * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(this.polygon.getX(), newY);
    }
    public void moveJoyRight(double speedK)
    {
        float newX =(float)(this.polygon.getX() + speed * speedK * Gdx.graphics.getDeltaTime());
        this.polygon.setPosition(newX, this.polygon.getY());
    }
    public void moveJoyLeft(double speedK)
    {
        float newX =(float)(this.polygon.getX() - speed * speedK * Gdx.graphics.getDeltaTime());
        this.polygon.setPosition(newX, this.polygon.getY());
    }
    public void moveJoyUp(double speedK)
    {
        float newY =(float)(this.polygon.getY() - speed * speedK * Gdx.graphics.getDeltaTime());
        this.polygon.setPosition(this.polygon.getX(), newY);
    }
    public void moveJoyDown(double speedK)
    {
        float newY =(float)(this.polygon.getY() + speed * speedK * Gdx.graphics.getDeltaTime());
        this.polygon.setPosition(this.polygon.getX(), newY);
    }
    public void moveAccellerometr()
    {
        float newX = this.polygon.getX() + speed *
                            Gdx.input.getAccelerometerY() *
                            Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(newX, this.polygon.getY());
        if(this.polygon.getX() + this.shipImg.getRegionWidth() > Settings.screen_width) {
            this.polygon.setPosition(Settings.screen_width - this.shipImg.getRegionWidth(),this.polygon.getY());
            return;
        }
        if(this.polygon.getX() < 0)
            this.polygon.setPosition(0,this.polygon.getY());
    }
    public void shooting(ShipGun gun)
    {
        gun.shooting();
    }

    private class HealthIndicator
    {
        com.admiral.galaxyracer.Interfaces.Indicator indicator;
        HealthIndicator(com.admiral.galaxyracer.Interfaces.Indicator indicator)
        {
            this.indicator = indicator;
        }

        void update()
        {
            indicator.update();
        }
    }
    private class ImageIndicator implements com.admiral.galaxyracer.Interfaces.Indicator
    {
        Texture frame = new Texture("health_frame.png");
        TextureRegion healthLine = new TextureRegion(new Texture("health_line.png"));
        double healthStep = healthLine.getRegionWidth() * 0.01;
        int healthPercents;

        @Override
        public void update() {
            this.healthPercents = (int)(Ship.this.health/Settings.shipHealth * 100);
            healthLine.setRegionWidth((int)(this.healthPercents * healthStep));

            float x = 1640;
            screen.draw(frame,x,Settings.screen_height - 80);
            screen.draw(healthLine,x,Settings.screen_height - 80);
        }
    }
    private class TextIndicator implements com.admiral.galaxyracer.Interfaces.Indicator
    {
        BitmapFont fontOriginal = Debugger.createFont();
        BitmapFont fontNormal = Debugger.createFont(0.08f,0.99f,0, 1,58);
        BitmapFont fontWarning = Debugger.createFont(0.92f, 0.98f, 0.12f, 1, 58);
        BitmapFont fontCritical = Debugger.createFont(0.90f,0,0,1,58);
        int healthPercents;

        @Override
        public void update() {
            this.healthPercents = (int)(Ship.this.health/Settings.shipHealth * 100);
            float x = 1570;
            float y = Settings.screen_height - 5;
            float step = 230;
            String string = Integer.toString(healthPercents) + "*";

            fontOriginal.draw(screen,"STRENGHT:",x, y);
            if(healthPercents > 65)
                this.fontNormal.draw(screen, string ,x + step, y);
            else if (healthPercents > 25)
                this.fontWarning.draw(screen, string ,x + step, y);
            else
                this.fontCritical.draw(screen, string ,x + step, y);
        }
    }
    private class ShipSplinters
    {
        ArrayList<Splinter> splinters = new ArrayList<Splinter>();

        class Splinter
        {
            private static final float fireTimeAlive = 115;
            private float frequencyFire = 50;
            private float fireTime;
            private boolean fireAlive;

            TextureRegion img;
            Polygon polygon;
            String animation;
            float speedX, speedY;
            float rotation;
            float stepRotation;

            Splinter(TextureRegion img, String animationName)
            {
                this.img = img;
                this.polygon = new Polygon(new float[]{0, 0, img.getRegionWidth(), 0, img.getRegionWidth(), img.getRegionHeight(), 0, img.getRegionHeight()});
                this.animation = animationName;
            }

            void update()
            {
                float newX = this.polygon.getX() + speedX * Gdx.graphics.getDeltaTime();
                float newY = this.polygon.getY() + speedY * Gdx.graphics.getDeltaTime();
                this.polygon.setPosition(newX,newY);
                this.polygon.setRotation(rotation);
                screen.draw(img,
                        polygon.getX(),
                        polygon.getY(),
                        img.getRegionWidth()/2,
                        img.getRegionHeight()/2,
                        img.getRegionWidth(),
                        img.getRegionHeight(),
                        1,
                        1,
                        rotation);
                rotation += stepRotation;
                if(fireAlive) fire();
            }
            private void fire()
            {
                this.fireTime += Gdx.graphics.getDeltaTime() * 1000;
                if(this.fireTime >= frequencyFire)
                {
                    if(frequencyFire > fireTimeAlive)
                        this.fireAlive = false;
                    // TODO Поменять анимацию огня
                    Controller.animations.createAnimation(animation, polygon.getX(),polygon.getY());
                    this.frequencyFire += 5;
                    this.fireTime = 0;
                }
            }
        }

        ShipSplinters()
        {
            for(int i = 0; i < 10; i++) {
                Splinter splinter = new Splinter(new TextureRegion(new Texture("ship_splinter" + (i + 1) + ".png")),"Burn");
                splinters.add(splinter);
            }
        }

        void start(float x, float y)
        {
            for(Splinter splinter : splinters) {
                splinter.fireAlive = true;
                splinter.frequencyFire = 50;
                splinter.polygon.setPosition(x, y);
                splinter.speedX = (Controller.speed * intersectPoint.normal.x) / 8 + Controller.speed * (-random.nextFloat() + random.nextFloat());
                splinter.speedX *= -1;
                splinter.speedY = (Controller.speed * intersectPoint.normal.y) / 8 + Controller.speed * (-random.nextFloat() + random.nextFloat());
                splinter.rotation = 0;
                splinter.stepRotation = random.nextInt(4) * (random.nextInt(3) - 1);
            }
        }
        void stop()
        {
            for(Splinter splinter : splinters) {
                splinter.polygon.setPosition(0, 0);
            }
        }
        void update()
        {
            for(Splinter splinter : splinters)
                splinter.update();
        }
    }
}

