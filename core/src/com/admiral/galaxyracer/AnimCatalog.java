package com.admiral.galaxyracer;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

public class AnimCatalog {
    private HashMap<String,TextureRegion[]> catalog;

    /*********** Каталог анимаций (сюда добавлять новые) ************/
    AnimCatalog() {
        catalog = new HashMap<String, TextureRegion[]>();
        catalog.put("Burn", fromAtlas("animation/burn"));
        catalog.put("MeteorBurnLarge1", fromAtlas("animation/met_burn1_1"));
        catalog.put("MeteorBurnLarge2", fromAtlas("animation/met_burn1_2"));
        catalog.put("MeteorBurnLarge3", fromAtlas("animation/met_burn1_3"));
        catalog.put("MeteorBurnMedium", fromAtlas("animation/met_burn2_1"));
        catalog.put("MeteorBurnSmall", fromAtlas("animation/met_burn3_1"));
        catalog.put("Shot", fromAtlas("animation/shot16"));
        catalog.put("ShipBurn",fromAtlas("animation/ship_burn"));
        catalog.put("ShipFire", fromAtlas("animation/ed_anim4"));

//        catalog.put("Burn", slicing("burn2.png",3,3));
//        catalog.put("MeteorBurnLarge1", slicing("met_burn1_1(2).png",3,3));
//        catalog.put("MeteorBurnLarge2", slicing("met_burn1_2.png",3,3));
//        catalog.put("MeteorBurnLarge3", slicing("met_burn1_3.png",3,3));
//        catalog.put("MeteorBurnMedium", slicing("met_burn2_1.png",2,3));
//        catalog.put("MeteorBurnSmall", slicing("met_burn3_1.png",2,3));
//        catalog.put("Shot", reverse(slicing("shot16.png",6,1)));
//        catalog.put("ShipBurn",slicing("ship_burn.png",1,6));
//        catalog.put("LabelOverheat", slicing("label_overheat.png",6,1));
//        catalog.put("ShipFire", slicing("ed_anim2.png",6,1));
    }

    /************ Нарезание кадров из анимационной карты ************/
    private TextureRegion[] slicing(String nameImg, int lines, int colums )
    {
        Texture img = new Texture(nameImg);
        TextureRegion[] animMap = new TextureRegion[lines*colums];
        int w = img.getWidth()/colums;
        int h = img.getHeight()/lines;

        for(int i = 0; i < lines; i++)
            for(int j = 0; j < colums; j++)
                animMap[j+i*colums] = new TextureRegion(img,w*j,h*i,w,h);

        return animMap;
    }

    private TextureRegion[] fromAtlas(String path) {

        String name = path + "/image_part";
        int size = 0;
        TextureAtlas.AtlasRegion exists;
        do
            exists = Controller.atlas.findRegion(name, ++size);
        while(exists != null);
        size--;

        if(size <= 0)
            return null;

        TextureRegion[] animMap = new TextureRegion[size];
        for(int i = 0; i < size; i++)
            animMap[i] = Controller.atlas.findRegion(name,i + 1);

        return animMap;
    }

    private TextureRegion[] reverse(TextureRegion[] map)
    {
        TextureRegion[] resultMap = new TextureRegion[map.length];
        for(int j = 0, i = map.length; --i >= 0; j++)
        {
           resultMap[j] = map[i];
        }
        return resultMap;
    }

    public TextureRegion[] getAnimMap(String name)
    {
        return catalog.get(name);
    }
}