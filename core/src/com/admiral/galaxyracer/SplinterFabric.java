package com.admiral.galaxyracer;

import com.badlogic.gdx.utils.Array;
import java.util.Random;

public class SplinterFabric {

    private Array tasks = new Array();
    private Random random = new Random();

    public SplinterFabric()
    {

    }

    public void createSplinters(String nameSplinter, float x, float y, int count)
    {
        tasks.add(new SplinterTask(nameSplinter, x, y, count));
    }
    private void clearRemains()
    {
        for(int i = tasks.size; --i >= 0;) {
            SplinterTask splinterTask = (SplinterTask) tasks.get(i);
            if(!splinterTask.alive)
                tasks.removeIndex(i);
        }
    }
    public void update()
    {
        //Debugger.log("TASKS: " + tasks.size);
        for(Object task : tasks) {
            SplinterTask splinterTask = (SplinterTask) task;
            splinterTask.update();
        }
        clearRemains();
    }

    private class SplinterTask
    {
        String name;
        float x, y;
        int count;
        boolean alive;

        SplinterTask(String nameSplinter, float x, float y, int count)
        {
            this.alive = true;
            this.name = nameSplinter;
            this.x = x;
            this.y = y;
            this.count = count;
        }

        void update()
        {
            if(--count > 0)
            {
                if(this.name.equals("MeteorSplinter"))
                {
                    int k = 4;
                    MeteorSplinter splinter = MeteorSplinter.meteorSplinterPool.obtain();
                    int kk = 1 + random.nextInt(5);
                    float speedX = (Controller.speed + (Controller.speed * random.nextFloat())) / kk;
                    float speedY = (Controller.speed + (Controller.speed * random.nextFloat())) / kk;

                    if(count % k == 0)
                        splinter.init(x, y, speedX, speedY);
                    else if(count % k == 1)
                        splinter.init(x, y, speedX, -speedY);
                    else if(count % k == 2)
                        splinter.init(x, y, -speedX, -speedY);
                    else if(count % k == 3)
                        splinter.init(x, y, speedX, speedY);

                    Controller.meteorSplinters.add(splinter);
                }
            }
            else
                this.alive = false;
        }

    }
}
