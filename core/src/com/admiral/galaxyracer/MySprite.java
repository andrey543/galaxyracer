package com.admiral.galaxyracer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Вождь on 09.09.2017.
 */

public abstract class MySprite extends Sprite{

    protected boolean alive;

    public abstract void update();



}
