package com.admiral.galaxyracer.Interfaces;

public interface Indicator {
    void update();
}
