package com.admiral.galaxyracer.Interfaces;

public interface Score {

    void writeRecord(String score);
    String readRecord();
}
