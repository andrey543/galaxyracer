package com.admiral.galaxyracer;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGame extends Game {
	public static OrthographicCamera camera = new OrthographicCamera();

	private SpriteBatch batch;
	private Controller controller;
	private com.admiral.galaxyracer.Interfaces.Score scoreListener;
	private com.admiral.galaxyracer.Menu menu;

	/****************** Начальная инициализация при запуске игры *****************************/
	@Override
	public void create () {
		this.batch = new SpriteBatch();
		this.controller = new Controller(batch);
		this.menu = com.admiral.galaxyracer.Menu.getInstance(batch,controller);

		menu.addScoreListener(scoreListener);
		camera.setToOrtho(false, com.admiral.galaxyracer.Settings.screen_width, com.admiral.galaxyracer.Settings.screen_height);
	}

	public void addScoreListener(com.admiral.galaxyracer.Interfaces.Score scoreListener)
	{
		this.scoreListener = scoreListener;
	}
	/********************* Главный цикл игры *****************************/
	@Override
	public void render () {
		camera.update();
		batch.setProjectionMatrix(MyGame.camera.combined);
		/* Отрисовка объектов на экране заключается между batch.begin() и batch.end() */
		/*--------- Начало ------------ */

			batch.begin();
			menu.update();
			batch.end();

		/*---------- Конец ------------ */

	}
	/***********************       Пауза        ****************************************/
	@Override
	public void pause()
	{
		this.menu.pause();
	}
	public boolean canExit()
	{
		return menu.canExit();
	}
	/*********************** При выходе из игры ****************************************/
	@Override
	public void dispose () {
		System.exit(0);
	}
}
