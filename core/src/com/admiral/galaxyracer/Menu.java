package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;


/**
 * Created by Вождь on 28.01.2018.
 */

/** Singleton */
public class Menu extends StateMachine {

    private static Menu instance;

    private final SpriteBatch screen;
    private final Sky sky;
    private final com.admiral.galaxyracer.Ship ship;
    private Texture startLabel;
    private Texture pauseLabel;
    private Controller controller;
    private BitmapFont font;
    private com.admiral.galaxyracer.Interfaces.Score scoreListener;

    private double record;
    private boolean canExit;

    private Menu(final SpriteBatch screen, final Controller controller)
    {
        this.screen = screen;
        this.controller = controller;
        this.sky = Sky.getInstance(screen);
        this.ship = com.admiral.galaxyracer.Ship.getInstance(screen);
        this.startLabel = new Texture("tap_to_start.png");
        this.pauseLabel = new Texture("pause.png");
        this.activeState = States.Stoped;
        this.canExit     = true;

        initFont();
    }

    private void initFont()
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/digital.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 58; // размер текста

        this.font = generator.generateFont(parameter);
        this.font.setColor(0.93f,0.95f,0.2f,1f); // золотой цвет

        generator.dispose();
    }
    public static Menu getInstance(final SpriteBatch screen, final Controller controller)
    {
        if(instance == null)
            instance = new Menu(screen, controller);
        return instance;
    }
    public void addScoreListener(com.admiral.galaxyracer.Interfaces.Score scoreListener)
    {
        this.scoreListener = scoreListener;
        this.record = readRecord();
    }

    public void writeRecord(double score)
    {
        if(scoreListener != null) {
            if(record < score)
                scoreListener.writeRecord(Debugger.formatDouble(score));
        }
        else
            Debugger.log("scoreListener is null...");
    }
    public double readRecord()
    {
        if(scoreListener == null) {
            Debugger.log("scoreListener is null...");
            return -1;
        }

        String value =  scoreListener.readRecord();
        value = value.replace(',', '.');
        return Double.valueOf(value);
    }

    private void drawStart() {

        sky.update();
        screen.draw(startLabel,
                    com.admiral.galaxyracer.Settings.screen_width/2 - (this.startLabel.getWidth()/2),
                    com.admiral.galaxyracer.Settings.screen_height/2 - (this.startLabel.getHeight()/2)
                    );
        this.font.draw(this.screen, "max distance: " + record + " seconds of light", 0,  com.admiral.galaxyracer.Settings.screen_height);


        if(Gdx.input.isTouched())
            this.start();

    }

    private void drawPause()
    {
        sky.pause();
        screen.draw(pauseLabel,
                com.admiral.galaxyracer.Settings.screen_width/2 - (this.pauseLabel.getWidth()/2),
                com.admiral.galaxyracer.Settings.screen_height/2 - (this.pauseLabel.getHeight()/2)
        );

        if(Gdx.input.isTouched())
            this.start();
    }
    public boolean canExit() {
        return canExit;
    }
    @Override
    public void stop() {
        if(this.activeState == States.Running)
            setActiveState(States.Stoped);
        this.record = readRecord();
        this.canExit = true;
    }

    @Override
    public void start() {
        setActiveState(States.Running);
        controller.startGame();
        this.canExit = false;
    }

    @Override
    public void pause()
    {
        if(this.activeState == States.Running)
            setActiveState(States.Paused);
        controller.pauseGame();
        this.canExit = true;
    }

    @Override
    public void update() {

        switch (this.activeState)
        {
            case Stoped:
            {
                this.drawStart();
                break;
            }
            case Running:
            {
                controller.updateGame();
                break;
            }
            case Paused:
            {
                this.drawPause();
                break;
            }
        }
    }


}
