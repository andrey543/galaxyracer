package com.admiral.galaxyracer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Pool;

import java.util.Random;

/**
 * Created by Андрей on 16.08.2018.
 */

public class Bonus extends com.admiral.galaxyracer.MySprite implements Pool.Poolable{

    static SpriteBatch screen;
    static com.admiral.galaxyracer.Ship ship;
    static final Pool<Bonus> pool = new Pool<Bonus>() {
        @Override
        protected Bonus newObject() {
            return new Bonus();
        }
    };
    static final Random random = new Random();
    static final Object[][] kinds =
            {
                    {com.admiral.galaxyracer.Controller.atlas.findRegion("bonuses/bonus1"),  new EffectPower()},
                    {com.admiral.galaxyracer.Controller.atlas.findRegion("bonuses/bonus2"), new EffectFrezee()},
                    {com.admiral.galaxyracer.Controller.atlas.findRegion("bonuses/bonus3"), new EffectShield()}
            };


    public Polygon polygon;
    private TextureRegion img = (TextureRegion) kinds[0][0];
    private Effect effect;

    public Bonus()
    {
        this.polygon = new Polygon(new float[]{0, 0, this.img.getRegionWidth(),0, this.img.getRegionWidth(), this.img.getRegionHeight(), 0, this.img.getRegionHeight()});
    }

    public void action()
    {
        effect.process();
    }

    public void init(float x, float y)
    {
        this.alive = true;

        int i = random.nextInt(kinds.length);
        this.img    = (TextureRegion) kinds[i][0];
        this.effect = (Effect)  kinds[i][1];

        this.polygon.setPosition(x, y);

    }
    @Override
    public void reset() {

        this.alive  = false;
        this.img    = null;
        this.effect = null;
        this.polygon.setPosition(0,0);
    }

    @Override
    public void update()
    {
        if(this.polygon.getY() + this.img.getRegionHeight() <= 0)
            this.alive = false;

        float newY = this.polygon.getY() - (com.admiral.galaxyracer.Controller.speed - 100) * Gdx.graphics.getDeltaTime();
        this.polygon.setPosition(this.polygon.getX(), newY);

        screen.draw(this.img, this.polygon.getX(), this.polygon.getY());

    }

    /* Базовый класс  */
    private interface Effect
    {
        void process();
    }

    private static class EffectPower implements Effect
    {
        @Override
        public void process()
        {
            if(!com.admiral.galaxyracer.Controller.effectPower.alive)
                com.admiral.galaxyracer.Controller.effectPower.start();
        }
    }

    private static class EffectFrezee implements Effect
    {
        @Override
        public void process()
        {
            if(!com.admiral.galaxyracer.Controller.effectFreeze.alive)
                com.admiral.galaxyracer.Controller.effectFreeze.start();
            else
                com.admiral.galaxyracer.Controller.effectFreeze.prolong();
        }
    }
    private static class EffectShield implements Effect
    {
        @Override
        public void process()
        {
            ship.shield.activate();
        }
    }

}
