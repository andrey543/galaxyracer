package com.admiral.galaxyracer;
/**
    Класс предназначен для управления движением
    групп объектов.
    В контексте данного проекта это управление:
    - пулями
    - пришельцами
    - метеоритами
    - и т.д.

    Добавлена потокобезопасность(мютекс)
 */
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;

/**
 * Created by Вождь on 09.09.2017.
 */

public class Group<T extends MySprite> extends Array<T> {

    protected Pool<T> pool;
    public boolean isUpdate;
    private Timer timer = new Timer();

    public Group(final Pool<T> pool)
    {
        Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                com.admiral.galaxyracer.Debugger.log(pool.getClass().getName() + ": " + size);
            }
        };
        //timer.scheduleTask(task,1,0.7f);
        this.pool = pool;
    }

    private void clearRemains()
    {
        for(int i = this.size; --i >= 0;)
        {
            T sprite = this.get(i);
            if(!sprite.alive)
            {
                this.removeIndex(i);
                this.pool.free(sprite);
            }
        }
    }
    public void gameOver()
    {
        for(T t : this)
            pool.free(t);
        pool.clear();
        this.clear();
    }
    public synchronized void update()
    {
        isUpdate = true;

        for (T t: this)
            t.update();
        //Очистка неактивных объектов
        clearRemains();

        isUpdate = false;
    }
}
