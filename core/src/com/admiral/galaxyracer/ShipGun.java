package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by andrey on 27.04.18.
 */

public class ShipGun {
    
    private Sound sound = Gdx.audio.newSound(Gdx.files.internal("sounds/kos.mp3"));
    private com.admiral.galaxyracer.Ship ship;
    private float x;
    private float y;
    private float connectX;
    private float connectY;
    private int heat;
    private float shootingTime;
    private float coolingTime;
    private boolean isShooting;
    private boolean isRecharged;


    public static final Pool<ShipBullet>  bulletPool = new Pool<ShipBullet>() {
        @Override
        protected ShipBullet newObject() {
            return new ShipBullet();
        }
    };
    public static com.admiral.galaxyracer.Group<ShipBullet> bullets = new com.admiral.galaxyracer.Group<ShipBullet>(bulletPool);


    ShipGun(com.admiral.galaxyracer.Ship ship, float x, float y)
    {
        this.x        = ship.polygon.getX();
        this.y        = ship.polygon.getY();
        this.connectX = x;
        this.connectY = y;
        this.ship     = ship;
    }

    public float getX()
    {
        return this.x;
    }
    public float getY()
    {
        return this.y;
    }
    public int getHeat(){return this.heat;}

    private void shot()
    {
        ShipBullet bullet = bullets.pool.obtain();
        bullet.init(this);
        bullets.add(bullet);
        this.sound.play(0.8f);
        this.heat++;

    }
    public void shooting()
    {
        this.isShooting = true;

        if(this.heat >= ShipBullet.allowHeatLvl || !this.isRecharged) return;

        com.admiral.galaxyracer.Controller.animations.createShot(ship, this.connectX - 14, this.connectY + 45);
        this.shot();

        this.isRecharged = false;
    }
    private void recharge()
    {
        this.shootingTime += Gdx.graphics.getDeltaTime() * 1000;

        if(this.shootingTime > ShipBullet.frequencyLvl) {
            this.isRecharged = true;
            this.shootingTime = 0;
        }

    }
    private void cooling()
    {
        this.coolingTime += Gdx.graphics.getDeltaTime() * 1000;
        if(this.coolingTime > ShipBullet.coolingTimeLvl && this.heat != 0) {
            this.heat--;
            this.coolingTime = 0;
        }
    }

    public void update()
    {
        bullets.update();
        this.x = ship.polygon.getX() + connectX;
        this.y = ship.polygon.getY() + connectY;

        if(!this.isShooting)
            this.cooling();
        if(!this.isRecharged)
            this.recharge();

        this.isShooting = false;
    }
    public void initFirstRun()
    {
        for(int i = 0; i < 10; i++) {
            ShipBullet bullet = bullets.pool.obtain();
            bullet.init(this);
            bulletPool.free(bullet);
        }
    }
    public void reset()
    {
        this.heat = 0;
        bullets.gameOver();
    }

}
