package com.admiral.galaxyracer;
/**
    Класс предназначен для создания анимаций и управления движением
    групп анимационных объектов.
    В контексте данного проекта это управление анимациями:
    - взрывов
    - столкновений
    - выстрелов
    - и т.д.
 */
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;

/**
 * Created by Вождь on 09.09.2017.
 */

public class AnimationGroup extends Array<Animation> {
    private static Pool<Animation> pool = new Pool<Animation>() {
        @Override
        protected Animation newObject() {
            return new Animation();
        }
    };
    Timer timer = new Timer();

    AnimationGroup()
    {
        Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                Debugger.log(size);
            }
        };
        //timer.scheduleTask(task, 1, 0.7f);
    }

    private void clearRemains()
    {
        for(int i = this.size; --i >= 0;)
        {
            Animation animation = this.get(i);
            if(!animation.alive)
            {
                this.removeIndex(i);
                this.pool.free(animation);
            }
        }
    }
    public void gameOver()
    {
        for(Animation t : this)
            pool.free(t);
        pool.clear();
        this.clear();
    }

    public void update()
    {
        for(Animation t : this)
            t.update();
        clearRemains();
    }

    public void createAnimation(String name, float x, float y)
    {
        Animation animation = this.pool.obtain();
        animation.init(name,x,y);
        this.add(animation);
    }
    public void createLabel(String name, float x, float y)
    {
        Animation animation = this.pool.obtain();
        animation.init(name,x,y);
        animation.setFrameTime(90);
        animation.setSpeedDrop(0);
        this.add(animation);
    }
    public void createShot(Ship ship, float offsetX, float offsetY)
    {
        Animation animation = this.pool.obtain();
        animation.init("Shot",ship.polygon.getX(),ship.polygon.getY());
        animation.fixedTo(ship, offsetX, offsetY);
        animation.setSpeedDrop(0);
        animation.setFrameTime(25);
        this.add(animation);
    }
    public void createShipFire(Ship ship, float offsetX, float offsetY)
    {
        Animation animation = this.pool.obtain();
        animation.init("ShipFire", ship.polygon.getX(), ship.polygon.getY());
        animation.fixedTo(ship,offsetX,offsetY);
        animation.setFrameTime(35);
        animation.setLooping(true);
        this.add(animation);
    }
    public void createMeteorBurn(String type,int ftime, float x, float y)
    {
        Animation animation = this.pool.obtain();
        animation.init(type,x,y);
        animation.setFrameTime(ftime);
        this.add(animation);
    }
    public void createShipBurn(float x, float y)
    {
        Animation animation = this.pool.obtain();
        animation.init("ShipBurn",x,y);
        animation.setFrameTime(50);
        this.add(animation);
    }
    // Уничтожит все анимации данного типа(name)
    public void destructionAnimation(String name)
    {
        for(Animation animation : this)
            if(animation.name.equals(name))
                animation.alive = false;
    }

    public static void initFirstRun()
    {
        for(int i = 0; i < Settings.meteorAllowed; i++) {
            Animation animation = pool.obtain();
            animation.init("ShipBurn", 0,0);
            pool.free(animation);
        }
    }

}
