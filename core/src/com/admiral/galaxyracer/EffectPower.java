package com.admiral.galaxyracer;

import com.badlogic.gdx.utils.Timer;

public class EffectPower
{
    public boolean alive;
    private Timer workTimer;
    private int time;
    private final int over = 5;
    private int currentSpeed;

    public EffectPower()
    {
        Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                time++;
            }
        };
        workTimer = new Timer();
        workTimer.scheduleTask(task,1,1);
        workTimer.stop();
    }

    public void start()
    {
        time  = 0;
        alive = true;
        currentSpeed = com.admiral.galaxyracer.Controller.speed;
        workTimer.start();
        com.admiral.galaxyracer.Controller.speed *= 2;
    }
    public void gameOver()
    {
        this.alive = false;
        com.admiral.galaxyracer.Controller.speed = com.admiral.galaxyracer.Settings.meteorSpeed;
    }
    private void stop()
    {
        com.admiral.galaxyracer.Controller.speed = com.admiral.galaxyracer.Controller.viewSpeed;
    }


    public void update()
    {
        if(time >= over) {
            alive = false;
            stop();
        }
    }
}
