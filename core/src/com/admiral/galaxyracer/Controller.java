package com.admiral.galaxyracer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.math.Intersector;

import java.util.Random;

/**
 * Created by Вождь on 27.08.2017.
 * Контроллер управляет всем функционалом(логикой) игры.
 * Контроллер управляет движением коробля с помощью акселлерометра, джостика и др. средств
 * управления, обрабатывает столкновения, стрельбу и генерацию игровых объектов
 */

public class Controller {

    public static TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("atlas.atlas"));
    public static com.admiral.galaxyracer.AnimationGroup animations = new com.admiral.galaxyracer.AnimationGroup();
    public static int speed                 = com.admiral.galaxyracer.Settings.meteorSpeed;
    public static int viewSpeed             = speed;
    public static EffectFreeze effectFreeze;
    public static EffectPower effectPower;
    public static Timer freqMeteorTimers[];
    public static Timer currentMeteorTimer;
    public static Timer startTimer;
    public static Timer speedTimer;
    public static Timer overTimer;
    public static Timer bonusTimer;
    public static final com.admiral.galaxyracer.SplinterFabric splinterFabric = new com.admiral.galaxyracer.SplinterFabric();
    public static final Group<MeteorSplinter> meteorSplinters = new Group<MeteorSplinter>(MeteorSplinter.meteorSplinterPool);
    private static final Group<Meteor> meteors = new Group<Meteor>(Meteor.meteorPool);
    private static final Group<Bonus> bonuses  = new Group<Bonus>(Bonus.pool);
    private static int meteorAllowed           = com.admiral.galaxyracer.Settings.meteorAllowed;
    private static final Music music           = Gdx.audio.newMusic(Gdx.files.internal("music/Floating Cities.mp3"));
    private static double time;
    private static long overTime;
    private final SpriteBatch screen;
    private final com.admiral.galaxyracer.Menu menu;
    private final Sprite background;
    private final com.admiral.galaxyracer.Ship ship;
    private final Random random = new Random();
    private final Sky sky;
    private BitmapFont font;

    Controller(SpriteBatch screen)
    {
        Meteor.screen       = screen;
        Bonus.screen        = screen;
        Animation.screen    = screen;
        com.admiral.galaxyracer.ShipBullet.screen   = screen;
        this.screen         = screen;

        this.sky            = Sky.getInstance(screen);
        this.background     = new Sprite(new Texture("3.jpg"));
        this.ship           = com.admiral.galaxyracer.Ship.getInstance(screen);
        effectFreeze        = new EffectFreeze(this.ship);
        effectPower         = new EffectPower();
        Bonus.ship          = this.ship;
        this.menu           = com.admiral.galaxyracer.Menu.getInstance(screen,this);

        this.background.setSize(Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());

        initFirstRun();
        initFont();
        initTimerTasks();
        music.play();
        music.setLooping(true);

    }
    /**
     ============================================
     ==      Алгоритм обработки столкновений   ==
     ============================================
     */
    private void collisions(){
        meteorsBullets();
        meteorsBackMeteors();
        meteorsSplinters();
        bonusShip();

        if(ship.alive) {
            meteorsShip();
            splintersShip();
        }
    }
    /** Коллизия метеоритов с пулями */
    private void meteorsBullets()
    {
        for(int i = 0; i < ShipGun.bullets.size; i++)
            for (int j = 0; j < meteors.size; j++) {
                com.admiral.galaxyracer.ShipBullet bullet = ShipGun.bullets.get(i);
                Meteor meteor = meteors.get(j);

                if(Intersector.overlapConvexPolygons(meteor.polygon,bullet.polygon))
                {
                    animations.createAnimation("Burn", bullet.polygon.getX()-21, bullet.polygon.getY());
                    meteor.decreaseHealth(bullet.power);

                    bullet.alive = false;
                    break;
                }
            }
    }
    /** Коллизия метеоритов с кораблем */
    private void meteorsShip()
    {
        for (Meteor meteor : meteors)
        {
            if (ship.shield.isAlive()) {
                if (Intersector.overlapConvexPolygons(ship.shield.getPolygon(), meteor.polygon)) {
                    meteor.pushBack(ship.shield.getPolygon().getX() + ship.shield.getTextureWidth() / 2);
                    break;
                }
            }
            if (Intersector.overlapConvexPolygons(ship.polygon, meteor.polygon, ship.intersectPoint)) {
                ship.decreaseHealth(meteor.getHealth());
                meteor.decreaseHealth(ship.getHealth());
                break;
            }
        }

    }
    private void splintersShip()
    {
        for (MeteorSplinter splinter : meteorSplinters) {
            if (ship.shield.isAlive()) {
                if (Intersector.overlapConvexPolygons(ship.shield.getPolygon(), splinter.polygon)) {
                    splinter.push();
                    break;
                }
            }
            if (Intersector.overlapConvexPolygons(ship.polygon, splinter.polygon)){
                ship.decreaseHealth(splinter.power);
                animations.createAnimation("Burn", splinter.polygon.getX(), splinter.polygon.getY());
                splinter.explosion();
                break;
            }
            for(com.admiral.galaxyracer.ShipBullet bullet : ShipGun.bullets)
            {
                if(Intersector.overlapConvexPolygons(bullet.polygon, splinter.polygon))
                {
                    animations.createAnimation("Burn", splinter.polygon.getX(), splinter.polygon.getY());
                    splinter.explosion();
                    bullet.alive = false;
                    break;
                }
            }
        }
    }
    private void bonusShip()
    {
        for(Bonus bonus : bonuses)
            if(Intersector.overlapConvexPolygons(bonus.polygon, ship.polygon))
            {
                bonus.action();
                bonus.alive = false;
            }
    }
    private void meteorsSplinters()
    {
        for (Meteor meteor : meteors)
            for (MeteorSplinter splinter : meteorSplinters) {
                if (Intersector.overlapConvexPolygons(splinter.polygon, meteor.polygon)) {
                    meteor.decreaseHealth(splinter.power);
                    splinter.explosion();
                }
            }
    }
    private void meteorsBackMeteors()
    {
        for(int i = 0; i < meteors.size; i++)
            for(int j = 0; j < meteors.size; j++)
            {
                Meteor meteor1 = meteors.get(i);
                Meteor meteor2 = meteors.get(j);

                if(meteor1 == meteor2 || !meteor1.isPushBack() && !meteor2.isPushBack())
                    continue;

                if(Intersector.overlapConvexPolygons(meteor1.polygon, meteor2.polygon)) {
                    float power1 = meteor1.getHealth();
                    float power2 = meteor2.getHealth();

                    meteor1.decreaseHealth(power2);
                    meteor2.decreaseHealth(power1);
                }
            }
    }

    /**
     ============================================
     =             Алгоритмы генерации          =
     ============================================
     */

    /** Генератор метеоритов */
    private void generationMeteors()
    {
        if(meteors.size < meteorAllowed)
        {
            if(time < 5) {
                if (random.nextInt(com.admiral.galaxyracer.Settings.meteorFrequency) == 1) {
                    createMeteor();
                }
            }
            else
            {
                createMeteor();
            }
        }
    }
    /** Новый метеор */
    private void createMeteor()
    {
        float x = random.nextInt(com.admiral.galaxyracer.Settings.screen_width);
        float y = com.admiral.galaxyracer.Settings.screen_height;
        Meteor meteor = Meteor.meteorPool.obtain();
        meteor.init(x,y);
        meteors.add(meteor);
    }
    public static void createMeteor(float x, float y)
    {
        Meteor meteor = Meteor.meteorPool.obtain();
        meteor.init(x,y);
        meteors.add(meteor);
    }

    public static void createBonus(float x, float y)
    {
        Bonus bonus = Bonus.pool.obtain();
        bonus.init(x,y);
        bonuses.add(bonus);
    }

    /**
     ============================================
     ==                 Счет                   ==
     ============================================
     */

    private void drawTime()
    {
        //Debugger.drawLog("time:", Debugger.formatDouble(time,"##0"), screen, 0, Settings.screen_height - 100);
        this.font.draw(this.screen,"distance: " + com.admiral.galaxyracer.Debugger.formatDouble((time * viewSpeed)/300000), 0, com.admiral.galaxyracer.Settings.screen_height - 5 );
        this.font.draw(this.screen,"time : " + (int)time, 0, com.admiral.galaxyracer.Settings.screen_height - 55);
        this.font.draw(this.screen,"speed: " + viewSpeed, 0, com.admiral.galaxyracer.Settings.screen_height - 105);
        //this.font.draw(this.screen,"score: " + Debugger.formatDouble((time * speed)/300000), 0,Settings.screen_height - 130);
        //this.font.draw(this.screen, "speed:" + speed, 1650, Settings.screen_height);
        //this.font.draw(this.screen, "fps:" + Gdx.graphics.getFramesPerSecond(), 1650, Settings.screen_height - 50);
    }

    /**
     ============================================
     ==      Главные функциии обновления       ==
     ============================================
     */
    private void update()
    {
        this.sky.update();
        this.generationMeteors();
        splinterFabric.update();
        meteors.update();
        this.ship.update();
        this.collisions();
        meteorSplinters.update();
        this.effects();
        bonuses.update();
        animations.update();

        this.drawTime();
        this.gameOver();
        this.startShip();
        this.changeMeteorTimer();

        int space = 80;

        //Debugger.drawLog("meteors:", meteorAllowed, screen, 0, Settings.screen_height/2);
        //Debugger.drawLog("speed:", ShipBullet.getSpeed(), screen, 0, Settings.screen_height/2 - space);
        //Debugger.drawLog("power:", ShipBullet.getPower(), screen, 0, Settings.screen_height/2 - space*2);
        //Debugger.drawLog("powerLvl:", ShipBullet.powerLvl, screen, 0, Settings.screen_height/2 - space*3);
        //Debugger.drawLog("Splinters:", meteorSplinters.size(), screen, 0, Settings.screen_height/2 - space*3);
        //Debugger.drawLog("Health:", ship.getHealth(), screen, 0, Settings.screen_height/2 - space*4);
        //Debugger.drawLog("freq:",ShipBullet.getFrequency(), screen, 0, Settings.screen_height/2 - space*5);
        //Debugger.profile(screen,0, Settings.screen_height/2);

    }

    public void updateGame()
    {
        update();
        userControl();
    }

    /**
     ============================================
     ==      Пользовательское управление       ==
     ============================================
     */
    private void userControl()
    {
        /** Управление перемещением корабля */
        if(ship.alive) {
            if (com.admiral.galaxyracer.Settings.android)
                this.ship.moveAccellerometr();
            else {
                if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                    ship.moveLeft();
                if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                    ship.moveRight();
                if (Gdx.input.isKeyPressed(Input.Keys.UP))
                    ship.moveUp();
                if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
                    ship.moveDown();
                /***************** ОТЛАДКА *******************/
                if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                    ship.shooting(ship.gun1);
                    ship.shooting(ship.gun2);
                }
                if (Gdx.input.isKeyJustPressed(Input.Keys.C))
                    ship.decreaseHealth(1);
                if (Gdx.input.isKeyJustPressed(Input.Keys.V))
                    ship.decreaseHealth(-1);
                if (Gdx.input.isKeyJustPressed(Input.Keys.F))
                    effectFreeze.start();
                if (Gdx.input.isKeyJustPressed(Input.Keys.P))
                    effectPower.start();
                if (Gdx.input.isKeyJustPressed(Input.Keys.S))
                    ship.shield.activate();
                if (Gdx.input.isKeyJustPressed(Input.Keys.B))
                    createBonus(com.admiral.galaxyracer.Settings.screen_width/2, com.admiral.galaxyracer.Settings.screen_height/2);
                    //createMeteor(Settings.screen_width / 2, Settings.screen_height / 2);
                if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
                    menu.pause();
            }
            /** Управление стрельбой */

            for(int i = 0; i < 2; i++) {
             if(Gdx.input.isTouched(i)) {

                 if (Gdx.input.getX(i) > Gdx.graphics.getWidth()/2) {
                     ship.shooting(ship.gun2);
                 }
                 if (Gdx.input.getX(i) < Gdx.graphics.getWidth()/2) {
                     ship.shooting(ship.gun1);
                 }
             }

            }
        }
    }
    private void effects()
    {
        if(effectFreeze.alive)
            effectFreeze.update();
        if(effectPower.alive)
            effectPower.update();

    }

    /**
     ============================================
     ====             Отрисовка            ======
     ============================================
     */

    private void initFont()
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/digital.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 58; // размер текста

        this.font = generator.generateFont(parameter);
        this.font.setColor(0.93f,0.95f,0.2f,1f); // золотой цвет

        generator.dispose();
    }
    /**
     ============================================
     ====       Событийные алгоритмы       ======
     ============================================
     */
    public void startGame()
    {
        if(ship.gameOver)
        {
            overTimer.start();
            return;
        }
        if(!ship.alive)
            ship.create();
        startTimer.start();
        speedTimer.start();
        currentMeteorTimer.start();
        bonusTimer.start();
    }
    private void stopGame()
    {
        double score = (time * viewSpeed)/300000;
        menu.writeRecord(score);

        currentMeteorTimer.stop();
        startTimer.stop();
        overTimer.stop();
        meteors.gameOver();
        bonuses.gameOver();
        meteorSplinters.gameOver();
        animations.gameOver();
        menu.stop();
        effectFreeze.stop();
        effectPower.gameOver();
        speed         = com.admiral.galaxyracer.Settings.meteorSpeed;
        meteorAllowed = com.admiral.galaxyracer.Settings.meteorAllowed;
        viewSpeed     = speed;
        time          = 0;
        overTime      = 0;
        ship.gameOver = false;
        com.admiral.galaxyracer.ShipBullet.gameOver();
    }
    public void pauseGame()
    {
        startTimer.stop();
        speedTimer.stop();
        currentMeteorTimer.stop();
        overTimer.stop();
        bonusTimer.stop();
    }
    public void startShip()
    {
        if(ship.polygon.getY() < com.admiral.galaxyracer.Settings.shipStartY && ship.alive) {
            float newY = ship.polygon.getY() + 250 * Gdx.graphics.getDeltaTime();
            float currentX = ship.polygon.getX();
            ship.polygon.setPosition(currentX, newY);
        }
    }
    // Функция для оптимизации первого запуска( загрузки в память первых обьектов )
    // Т.к. была проблема "тормозов" при первом запуске игры
    private void initFirstRun()
    {
        for(int i = 0; i < 10; i++)
        {
            Meteor meteor = meteors.pool.obtain();
            meteor.init(0,0);
            meteors.pool.free(meteor);
        }
        ship.gun1.initFirstRun();
        ship.gun2.initFirstRun();
        com.admiral.galaxyracer.AnimationGroup.initFirstRun();
    }
    // Функция инициализации таймер-событий
    private void initTimerTasks()
    {
        // Таймер одного раунда(до столкновения)
        Timer.Task startTime = new Timer.Task() {
            @Override
            public void run() {
                time += com.admiral.galaxyracer.Settings.stepTime;
            }
        };
        startTimer = new Timer();
        startTimer.scheduleTask(startTime,(float) com.admiral.galaxyracer.Settings.stepTime,(float) com.admiral.galaxyracer.Settings.stepTime);
        startTimer.stop();

        // Таймер увеличения скорости
        Timer.Task incrementSpeed = new Timer.Task() {
            @Override
            public void run() {
                speed += com.admiral.galaxyracer.Settings.meteorSpeedStep;
                viewSpeed += com.admiral.galaxyracer.Settings.meteorSpeedStep;
            }
        };
        speedTimer = new Timer();
        speedTimer.scheduleTask(incrementSpeed, com.admiral.galaxyracer.Settings.meteorAllowedIncInterval, com.admiral.galaxyracer.Settings.meteorAllowedIncInterval);
        speedTimer.stop();

        freqMeteorTimers = new Timer[com.admiral.galaxyracer.Settings.countMeteorTimers];
        for(int i = 0; i < freqMeteorTimers.length; i++)
        {
            // Таймер увеличения макс. кол-ва метеоров
            Timer.Task incrementAllowedMeteors = new Timer.Task() {
                @Override
                public void run() {
                    ++meteorAllowed;
                }
            };

            freqMeteorTimers[i] = new Timer();
            freqMeteorTimers[i].scheduleTask(incrementAllowedMeteors,
                    com.admiral.galaxyracer.Settings.meteorAllowedIncInterval * (i+1),
                    com.admiral.galaxyracer.Settings.meteorAllowedIncInterval * (i+1));
            freqMeteorTimers[i].stop();
        }
        currentMeteorTimer = freqMeteorTimers[0];

        Timer.Task gameOverTime = new Timer.Task() {
            @Override
            public void run() {
                ++overTime;
            }
        };
        overTimer = new Timer();
        overTimer.scheduleTask(gameOverTime, 1, 1);
        overTimer.stop();

        Timer.Task generationBonus = new Timer.Task() {
            @Override
            public void run() {
                createBonus(random.nextInt(com.admiral.galaxyracer.Settings.screen_width), com.admiral.galaxyracer.Settings.screen_height);
            }
        };
        bonusTimer = new Timer();
        bonusTimer.scheduleTask(generationBonus, 1, com.admiral.galaxyracer.Settings.bonusIntervalGeneration);
        bonusTimer.stop();
    }
    private void changeMeteorTimer()
    {
        int int_time = (int)time;
        if(int_time % com.admiral.galaxyracer.Settings.stepMeteorTimers == 0 && int_time != 0 ) {
            int index = (int)int_time/ com.admiral.galaxyracer.Settings.stepMeteorTimers;
            if(index < freqMeteorTimers.length) {
                currentMeteorTimer.stop();
                currentMeteorTimer = freqMeteorTimers[index];
                currentMeteorTimer.start();
            }
        }
    }
    private void gameOver()
    {
        if(overTime > 3)
            stopGame();
    }
}
