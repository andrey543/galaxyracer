## GALAXY RACER

[Google play](https://play.google.com/store/apps/details?id=com.admiral.galaxyracer&hl=ru&gl=US)

![Icon](android/res/mipmap-xxxhdpi/ic_launcher.png)

### Pass as far as possible on the space, high-speed ship in the arcade game Galaxy Racer.
### To survive in a cold and lonely space, use:
- two guns, opening the fire from the left and right side
- acceleration and deceleration of time
- power shields to reflect lumps
- and of course, own skill :)